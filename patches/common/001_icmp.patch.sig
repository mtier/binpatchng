untrusted comment: signature from openbsd 5.5 base secret key
RWRGy8gxk9N931I5zFR8QzfeyD2OQkGJHvzdIeM+94h1HSePXfTzt2eojrqu3AiQ2CHnuS69+livrtk60oBnbrRPAR5dhPtf7wQ=

OpenBSD 5.5 errata 1, Mar 15, 2014:  Memory corruption happens during
ICMP reflection handling.  ICMP reflection is disabled by default.

Apply patch using:
    
    signify -Vep /etc/signify/openbsd-55-base.pub -x 001_icmp.patch.sig -m - | \
        (cd /usr/src && patch -p0)

Then build and install a new kernel:

    cd /usr/src/sys/arch/`machine`/conf
    KK=`sysctl -n kern.osversion | cut -d# -f1`
    config $KK
    cd ../compile/$KK
    make
    make install

Index: sys/netinet/ip_icmp.c
===================================================================
RCS file: /cvs/src/sys/netinet/ip_icmp.c,v
retrieving revision 1.114
diff -u -p -r1.114 ip_icmp.c
--- sys/netinet/ip_icmp.c	19 Jan 2014 05:01:50 -0000	1.114
+++ sys/netinet/ip_icmp.c	14 Mar 2014 16:24:03 -0000
@@ -611,9 +611,12 @@ reflect:
 		memset(&ssrc, 0, sizeof(ssrc));
 		sdst.sin_family = sgw.sin_family = ssrc.sin_family = AF_INET;
 		sdst.sin_len = sgw.sin_len = ssrc.sin_len = sizeof(sdst);
-		memcpy(&sdst.sin_addr, &icp->icmp_ip.ip_dst, sizeof(sdst));
-		memcpy(&sgw.sin_addr, &icp->icmp_gwaddr, sizeof(sgw));
-		memcpy(&ssrc.sin_addr, &ip->ip_src, sizeof(ssrc));
+		memcpy(&sdst.sin_addr, &icp->icmp_ip.ip_dst,
+		    sizeof(sdst.sin_addr));
+		memcpy(&sgw.sin_addr, &icp->icmp_gwaddr,
+		    sizeof(sgw.sin_addr));
+		memcpy(&ssrc.sin_addr, &ip->ip_src,
+		    sizeof(ssrc.sin_addr));
 
 #ifdef	ICMPPRINTFS
 		if (icmpprintfs) {
