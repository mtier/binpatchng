PKG_SIGN_STRING=-s signify -s /etc/signify/mtier-58-pkg.sec

OSREV=5.8
KERNEL=GENERIC.MP GENERIC
PARALLEL_BUILD=yes

MAINTAINER="m:tier <info@mtier.org>"

# List of patches for userland/X11
PATCH_COMMON=

# List of kernel patches
PATCH_KERNEL=

# Define type of rollback support ('all' or 'kernel'), set to 'none' to disable.
ROLLBACK=all

#===========================================================================
# The patch targets start here.
#
# Care should be taken for writing valid rules for make.
# The instructions must be taken from the top of each patch file, with some
# rewrites:
#
# - Paths relative to src/ must be made relative to ${WRKSRC}/
# - make targets must be changed for their counterparts provided by binpatch:
#	make obj:		${_obj}
#	make cleandir:		${_cleandir}
#	make depend:		${_depend}
#	make install:		${_install}
#	make && make install:	${_build}
#
#	make -f Makefile.bsd-wrapper obj:		${_obj_wrp}
#	make -f Makefile.bsd-wrapper cleandir:		${_cleandir_wrp}
#	make -f Makefile.bsd-wrapper depend:		${_depend_wrp}
#	make -f Makefile.bsd-wrapper && \
#		make -f Makefile.bsd-wrapper install:	${_build_wrp}
#	make -f Makefile.bsd-wrapper install:		${_install_wrp}
#
#

.include "mk/bsd.binpatch.mk"
